﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestSelenium
{
    class Program
    {

        static string[] roles = new[] { "driver", "sweeper", "cook", "cleaner", "baby sitter" };
        static string[] durations = new[] { "I don't know!", "How does it matter", "why do you need it" };
        static string[] comments = new[] { "Have Fun!", "Chill!", "Enjoy!" };
        static int minRefCode = 60000;
        static int maxRefCode = 89999;

        static void Main(string[] args)
        {
            startLoop();
        }

        static void startLoop()
        {
            var driver = new ChromeDriver();
            for (int i = minRefCode; i < maxRefCode; i++)
            {
                driver.Url = "https://docs.google.com/forms/d/e/1FAIpQLSeCmH8scj-t1cOvvsnyNX-ag0j8wc6_QeXMci-t2R4m0-t4yw/viewform?usp=sf_link";
                driver.Navigate();
                var role = roles[getRandomValue(0, 4)];
                var duration = durations[getRandomValue(0, 2)];
                var number = driver.FindElementsByCssSelector(".quantumWizTextinputPaperinputInput.exportInput");
                var comment = comments[getRandomValue(0, 2)];
                number[0].SendKeys(i.ToString());
                number[1].SendKeys(role);
                number[2].SendKeys(duration);
                var options = driver.FindElementsByCssSelector(".docssharedWizToggleLabeledContainer.freebirdFormviewerViewItemsRadioChoice");
                foreach (var item in options)
                {
                    item.Click();
                }
                var commentsBox = driver.FindElementByCssSelector(".quantumWizTextinputPapertextareaInput.exportTextarea");
                commentsBox.SendKeys(comment);
                var submitButton = driver.FindElementByCssSelector(".freebirdFormviewerViewNavigationButtons");
                submitButton.Click();
            }
            driver.Close();
            driver.Quit();
        }

        static int getRandomValue(int min, int max)
        {
            var random = new Random(DateTime.Now.Second);
            return random.Next(min, max);
        }
    }
}
